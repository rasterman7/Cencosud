# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui                                                                                 # Importa las librerias de PyQt y QGIS
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtXml import *
from qgis.gui import *
from qgis.core import *
from qgis.utils import *
from generalus import *                                                                                         # Importa la libreria funciones generales
from cencosud import *                                                                                          # Importa el archivo del plugin
from templating import *                                                                                        # Importa las librería de impresión y direccionamiento
import os.path

logsel = logDate(os.path.dirname(__file__), "Cencosud")                                                         # Toma una variable global para la instancia de log

def exportClientesCheck(ifacex, dicx, plug_dir):                                                                # <<<Función exportClientesCheck>>>
    global logsel
    logsel.info("Exportar Clientes - Inicio")
    if dicx['selClientes'].exportRadio2.isChecked():                                                            # Designa la acción a realizar en base a la opción de exportación seleccionada
        exportClientesToMap(ifacex, dicx, plug_dir)
    elif dicx['selClientes'].exportRadio1.isChecked():
        exportClientesToCSV(ifacex)
    else:
        logsel.info("Opción no seleccionada")
        msgWarning(ifacex, "Seleccionar Clientes", "No se ha seleccionado una opción de exportación")           # Muestra un mensaje de advertencia en caso de no haber seleccionado ninguna
    logsel.info("Exportar Clientes - Fin\n")

def exportClientesToCSV(ifacex):                                                                                # <<<Función exportClientesToCSV>>>
    global logsel
    logsel.info("Exportación a CSV")
    lyr1 = obtainLyr(ifacex, "Clientes")                                                                        # Verifica la presencia de la capa Clientes
    if lyr1 is not None:
        logsel.info("Generando capa de selección")
        lyr2 = copyMemLyrSelected(lyr1)                                                                         # Si está, genera una capa auxiliar con los clientes seleccionados
        QgsMapLayerRegistry.instance().addMapLayer(lyr2)
        logsel.info("Localizando el directorio de guardado")
        outty = fileDlg(ifacex, 'Tabla CSV', "Valores separados por comas [CSV] [OGR] (*.csv *.CSV);;", True)   # Le pide al usuario el directorio de guardado del archivo CSV
        if outty != '':                                                                                         # Verifica que el cuadro de direcciones no haya sido cerrado
            logsel.info("Guardando la capa como CSV")
            error = QgsVectorFileWriter.writeAsVectorFormat(lyr2,                                               # Si no lo fue, guarda la capa auxiliar en formato CSV y muestra un mensaje de éxito
            outty, 'utf-8', None, 'CSV', layerOptions=['SEPARATOR=SEMICOLON'])
            del error
            msgInfo(ifacex, "Seleccionar Clientes", "El archivo CSV ha sido guardado con éxito")
        QgsMapLayerRegistry.instance().removeMapLayer(lyr2.id())                                                # Elimina la capa auxiliar
    else:
        logsel.info("Capa Clientes no encontrada")
        msgError(ifacex, "La capa Clientes no se encuentra en el proyecto")                                     # Muestra un mensaje de error en caso de no hallarse la capa Clientes

def exportClientesToMap(ifacex, dicx, plug_dir):                                                                # <<<Función exportClientesToMap>>>
    global logsel
    dicx['selClientes'].hide()                                                                                  # Esconde el cuadro de diálogo
    logsel.info("Exportación a Mapa Temático")
    lyr1 = obtainLyr(ifacex, "Locales")                                                                         # Verifica la presencia de la capa Locales
    if lyr1 is not None:
        lyr2 = obtainLyr(ifacex, "Clientes")                                                                    # Si está, verifica la presencia de la capa Clientes
        if lyr2 is not None:
            if (len(lyr2.selectedFeatures()) > 0):                                                              # Si está, verifica que haya clientes seleccionados en la capa
                logsel.info("Generando las capas auxiliares")
                setCRS(ifacex)                                                                                  # Si los hay, asigna el correcto sistema de referencia de coordenadas
                (lst, rubb) = genExportAuxLayers(ifacex, dicx, lyr1, lyr2)                                      # Genera las capas auxiliares para la exportación y toma el radio de selección
                extent = rubb.asGeometry().boundingBox()
                rubb.reset()
                lyr_lst = lst[:]                                                                                # Toma una lista con las capas auxiliares y la raster de Open Layers (si está presente)
                op_lyr = obtainLyr(ifacex, "OSM Humanitarian Data Model")
                if op_lyr is not None: lyr_lst.append(op_lyr.id())
                logsel.info("Llamando al compositor de impresión")
                filex = os.path.join(os.path.dirname(__file__), "resources", "templates",                       # Llama al compositor de impresión con el directorio de la plantilla
                "standard_map_radius.qpt")
                comp = printingComposer(ifacex, lyr_lst, filex)
                logsel.info("Editando la plantilla del mapa")
                titlex = dicx['selClientes'].local_nombre.currentText()                                         # Configura las etiquetas, mapa y barra de escala del compositor
                sc_lst = [750, 1000, 1250, 1500, 1750, 2000, 2500, 3000, 4000, 5000, 6250, 7500,
                8750, 10000, 12500, 15000, 17500, 20000, 22500, 25000]
                setComposerStdFeats(comp, titlex, lyr_lst, extent, sc_lst)
                rad_label = comp.getComposerItemById("radius")                                                  # Configura la etiqueta del radio de la planilla
                radio = dicx['selClientes'].clientes_radio.currentText()
                rad_label.setText("RADIO: " + str(radio[0]) + "km")
                logsel.info("Exportando la plantilla a PDF")
                p_flag = compToPDF(ifacex, comp)                                                                # Exporta el compositor a un archivo PDF
                if p_flag:
                    logsel.info("Exportación completada exitosamente")
                    msgInfo(ifacex, "Seleccionar Clientes", "El mapa ha sido exportado con éxito" +             # Si el archivo fue guardado, muestra un mensaje de éxito
                    " al directorio indicado", 4)
                for l in lst: QgsMapLayerRegistry.instance().removeMapLayer(l)                                  # Elimina las capas auxiliares del proyecto
            else:
                logsel.info("No hay clientes seleccionados")
                msgWarning(ifacex, "Seleccionar Clientes", "No hay clientes seleccionados")                     # Muestra un mensaje de advertencia en caso de no haber clientes seleccionados
        else:
            logsel.info("Capa Clientes no encontrada")
            msgError(ifacex, "No se encontró la capa Clientes")                                                 # Muestra un mensaje de error en caso de no hallarse la capa Clientes
    else:
        logsel.info("Capa Locales no encontrada")
        msgError(ifacex, "No se encontró la capa Locales")                                                      # Muestra un mensaje de error en caso de no hallarse la capa Locales
    dicx['selClientes'].show()                                                                                  # Muestra nuevamente el cuadro de diálogo

def genExportAuxLayers(ifacex, dicx, lyr1, lyr2):                                                               # <<<Función genExportAuxLayers>>>
    if (len(lyr1.selectedFeatures()) != 1):                                                                     # En caso de no haber un local seleccionado, selecciona el indicado
        locales = obtainFeatByAttr(lyr1, u'Nombre', dicx['selClientes'].local_nombre.currentText())
        lyr1.setSelectedFeatures([locales[0].id()])
    lyr3 = copyMemLyrSelected(lyr1)                                                                             # Crea capas auxiliares para el local y los clientes seleccionados y las agrega al mapa
    lyr4 = copyMemLyrSelected(lyr2)
    QgsMapLayerRegistry.instance().addMapLayer(lyr3)
    QgsMapLayerRegistry.instance().addMapLayer(lyr4)
    local = list(lyr3.getFeatures())                                                                            # Etiqueta el local de la capa auxiliar
    exp = genLabelExpForFeat(lyr3, local[0])
    setLyrLabelModel(ifacex, lyr3, u'Nombre', False, None, exp)
    changeLyrColor(ifacex, lyr3.name(), "red")                                                                  # Asigna el color rojo a la capa del local y azul a la de clientes
    changeLyrColor(ifacex, lyr4.name(), "blue")
    lyr5 = newMemLyr("Polygon", "Buffers", [])                                                                  # Crea una capa auxiliar adicional para el radio de selección
    QgsMapLayerRegistry.instance().addMapLayer(lyr5, False)
    root = QgsProject.instance().layerTreeRoot()
    nodex = root.addLayer(lyr5)
    buff_lst = [it for it in ifacex.mapCanvas().scene().items() if type(it) == QgsRubberBand]                   # Toma el radio de selección. En caso de no existir, lo genera
    radio = dicx['selClientes'].clientes_radio.currentText()
    if len(buff_lst) == 0: rubb = genSelectRubberBand(ifacex, local, radio)
    else: rubb = buff_lst[0]
    feat = QgsFeature(lyr5.pendingFields())                                                                     # Agrega el radio a la capa como objeto y le da un color adecuado
    feat.setGeometry(rubb.asGeometry())
    lyr5.dataProvider().addFeatures([feat])
    changeLyrColor(ifacex, "Buffers", "darkgreen", 75)
    refreshAll(ifacex)                                                                                          # Actualiza el mapa
    lst = []                                                                                                    # Toma los identificadores de las capas creadas en una lista y la devuelve con el radio de selección
    lst.append(lyr3.id())
    lst.append(lyr4.id())
    lst.append(lyr5.id())
    return (lst, rubb)

def genSelectRubberBand(ifacex, local, radio):                                                                  # <<<Función genSelectRubberBand>>>
    global logsel
    logsel.info("Generando el radio de selección")
    factor = QgsUnitTypes().fromUnitToUnitFactor(QGis.Meters, ifacex.mapCanvas().mapUnits())                    # Genera el factor de conversión entre la unidad del mapa y el metro
    if radio[0] == '1': buffer = local.geometry().buffer(1000*factor, 10)                                       # Usa el factor para generar un buffer alrededor del local de tamaño según el indicado
    else: buffer = local.geometry().buffer(3000*factor, 10)
    rubb = QgsRubberBand(ifacex.mapCanvas(), False)                                                             # Genera la figura del radio de selección con el buffer, establece su formato y la devuelve
    colorx = QColor("darkgreen")
    colorx.setAlpha(100)
    rubb.setColor(colorx)
    rubb.setBrushStyle(Qt.Dense4Pattern)
    rubb.setToGeometry(buffer, None)
    return rubb

def selClientesByAttr(ifacex, dicx):                                                                            # <<<Función selClientesByAttr>>>
    global logsel
    logsel.info("Seleccionar Clientes - Selección de clientes por atributos")
    lyr1 = obtainLyr(ifacex, u'Locales')                                                                        # Verifica la presencia de la capa Locales
    if lyr1 is not None:
        lyr2 = obtainLyr(ifacex, u'Clientes')                                                                   # Si está, verifica la presencia de la capa Clientes
        if lyr2 is not None:
            lst = obtainFeatByAttr(lyr1, u'Nombre', dicx['selClientes'].local_nombre.currentText())             # Si está, verifica que el local seleccionado exista en la capa Locales
            if len(lst) > 0:
                logsel.info("Filtrando los clientes por sus atributos")
                attrdic = {}                                                                                    # Si existe, genera un diccionario con los filtros, en caso de no ser indistintos (sin el margen)
                if dicx['selClientes'].clientes_local.currentText() != u'Indistinto':
                    attrdic[u'local'] = dicx['selClientes'].clientes_local.currentText()
                if dicx['selClientes'].clientes_cadena.currentText() != u'Indistinto':
                    attrdic[u'cadena'] = dicx['selClientes'].clientes_cadena.currentText()
                if dicx['selClientes'].clientes_idrango.currentText() != u'Indistinto':
                    attrdic[u'idrango'] = dicx['selClientes'].clientes_idrango.currentText()
                if dicx['selClientes'].clientes_estado.currentText() != u'Indistinto':
                    attrdic[u'estado'] = dicx['selClientes'].clientes_estado.currentText()
                lst2 = obtainFeatByAttrDic(lyr2, attrdic)                                                       # Filtra los clientes de la capa por el diccionario generado
                marmax = None
                if not dicx['selClientes'].marmax_check.isChecked():
                    try: marmax = float(dicx['selClientes'].marmax_line.text())
                    except:
                        logsel.info("Número máximo de margen inválido")
                        msgWarning(ifacex, "Seleccionar Clientes", "El número ingresado para el margen" +
                        " máximo no es válido")
                marmin = None
                if not dicx['selClientes'].marmin_check.isChecked():
                    try: marmin = float(dicx['selClientes'].marmin_line.text())
                    except:
                        logsel.info("Número mínimo de margen inválido")
                        msgWarning(ifacex, "Seleccionar Clientes", "El número ingresado para el margen" +
                        " mínimo no es válido")
                lst3 = filterFeatByAttrRange(lst2, "Margen", marmin, marmax)
                selClientesByBuffer(ifacex, lst[0], lst3, dicx['selClientes'].clientes_radio.currentText())     # Llama a la función consecutiva para filtrar los resultantes en base al radio del local
            else:
                logsel.info("Local seleccionado inexistente")
                msgError(ifacex, "El local seleccionado no se encuentra en la capa Locales")                    # Muestra un mensaje de error en caso de no existir el local en la capa Locales
                selClientesComboBox(ifacex, dicx)                                                               # Actualiza la listas desplegables del cuadro de diálogo
        else:
            logsel.info("Capa Cientes no encontrada")
            msgError(ifacex, "No se encontró la capa Clientes en el proyecto")                                  # Muestra un mensaje de error en caso de no hallarse la capa Clientes
    else:
        logsel.info("Capa Locales no encontrada")
        msgError(ifacex, "No se encontró la capa Locales en el proyecto")                                       # Muestra un mensaje de error en caso de no hallarse la capa Locales
    logsel.info("Seleccionar Clientes - Fin\n")

def selClientesByBuffer(ifacex, local, clientes, radio):                                                        # <<<Función selClientesByBuffer>>>
    global logsel
    setCRS(ifacex)                                                                                              # Establece el CRS del proyecto y genera el radio de selección
    rubb = genSelectRubberBand(ifacex, local, radio)
    lst = []
    logsel.info("Filtrando los clientes por su proximidad al local")
    for f in clientes:                                                                                          # Filtra los clientes por su distancia al local (contención en el buffer)
        if rubb.asGeometry().contains(f.geometry()): lst.append(f.id())
    if len(lst) == 0:                                                                                           # Verifica si tras el último filtro quedó algún cliente
        logsel.info("No hay clientes restantes para el criterio dado")
        rubb.reset()                                                                                            # De no ser así, resetea el radio de selección y muestra un mensaje de advertencia
        msgWarning(ifacex, "Seleccionar Clientes", "No se encontraron clientes que cumplan " +
        "con el cirterio de selección")
    else:
        logsel.info("Seleccionando los clientes restantes")
        lyr = obtainLyr(ifacex, u'Clientes')                                                                    # De ser así, selecciona los clientes resultantes
        lyr.setSelectedFeatures(lst)
        for it in ifacex.mapCanvas().scene().items():                                                           # Elimina los radios de selección anteriores (en caso de haber alguno)
            if (type(it) == QgsRubberBand and it != rubb): it.reset()
        ext = rubb.asGeometry().boundingBox()                                                                   # Establece la extensión del mapa con el radio de selección y lo extiende por la etiqueta
        ext.grow(0.001)
        ifacex.mapCanvas().setExtent(ext)
        logsel.info("Etiquetando el local dado")
        lyr2 = obtainLyr(ifacex, u'Locales')                                                                    # Genera la etiqueta del local para que se distinga del resto por su nombre y lo selecciona
        lyr2.setSelectedFeatures([local.id()])
        exp = genLabelExpForFeat(lyr2, local)
        setLyrLabelModel(ifacex, lyr2, u'Nombre', False, None, exp)
    refreshAll(ifacex)                                                                                          # Actualiza el mapa

def selClientesComboBox(ifacex, dicx):                                                                          # <<<Función selClientesComboBox>>>
    global logsel
    logsel.info("Seleccionar Clientes - Actualización de las listas desplegables")
    attrToComboBoxCheck(ifacex, u'Locales', dicx['selClientes'].local_nombre, u'Nombre')                        # Actualiza las listas de los atributos de las capas Locales y Clientes, en caso de existir
    attrToComboBoxCheck(ifacex, u'Clientes', dicx['selClientes'].clientes_local, u'local')
    attrToComboBoxCheck(ifacex, u'Clientes', dicx['selClientes'].clientes_cadena, u'cadena')
    attrToComboBoxCheck(ifacex, u'Clientes', dicx['selClientes'].clientes_idrango, u'idrango')
    attrToComboBoxCheck(ifacex, u'Clientes', dicx['selClientes'].clientes_estado, u'estado')
    dicx['selClientes'].clientes_local.insertItem(0, u'Indistinto')                                             # A cada lista le añade el campo Indistinto significando que no hay filtro
    dicx['selClientes'].clientes_cadena.insertItem(0, u'Indistinto')
    dicx['selClientes'].clientes_idrango.insertItem(0, u'Indistinto')
    dicx['selClientes'].clientes_estado.insertItem(0, u'Indistinto')
    dicx['selClientes'].marmin_line.clear()                                                                     # Deja en blanco las líneas de margen
    dicx['selClientes'].marmax_line.clear()

def updateLocalesComboBox(ifacex, dicx, namex):                                                                 # <<<Función updateLocalesComboBox>>>
    lyr = obtainLyr(ifacex, u'Locales')                                                                         # Si la capa Locales existe, actualiza la etiqueta del local seleccionado en la lista desplegable
    if lyr is not None:
        lst = obtainFeatByAttr(lyr, u'Nombre', namex)
        if len(lst) > 0: dicx['selClientes'].local_cadena.setText(str(lst[0].attribute(u'Cadena')))

def updateMaxMargin(dicx, state):
    if state == 0: dicx['selClientes'].marmax_line.setEnabled(True)
    else: dicx['selClientes'].marmax_line.setEnabled(False)

def updateMinMargin(dicx, state):
    if state == 0: dicx['selClientes'].marmin_line.setEnabled(True)
    else: dicx['selClientes'].marmin_line.setEnabled(False)