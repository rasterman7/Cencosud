# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui                                                                                 # Importa las librerias de PyQt y QGIS
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtXml import *
from qgis.gui import *
from qgis.core import *
from qgis.utils import *
from generalus import *                                                                                         # Importa las funciones generales
import time                                                                                                     # Importa las librerias de fecha y tiempo
from datetime import date

def compToPDF(ifacex, comp):                                                                                    # <<<Función compToPDF>>>
    printer = QPrinter()                                                                                        # Llama a una instancia de impresora virtual
    printer.setOutputFormat(QPrinter.PdfFormat)                                                                 # Establece el formato de la impresora a PDF
    outty = fileDlg(ifacex, "mapa como PDF", "Adobe Acrobat Document(*.pdf *.PDF);;", True)                     # Le pide al usuario que indique el directorio del archivo de salida
    if outty == '': return False                                                                                # Verifica si el cuadro de diálogo fue cerrado
    else:
        printer.setOutputFileName(outty)
        printer.setPaperSize(QSizeF(comp.paperWidth(), comp.paperHeight()), QPrinter.Millimeter)                # Define el tamaño de la impresión y asigna la pantalla completa
        printer.setFullPage(True)
        printer.setColorMode(printer.colorMode())                                                               # Define el color y la resolución de la impresion
        printer.setResolution(comp.printResolution())
        pdfPainter = QPainter(printer)                                                                          # Llama al pintor del PDF, definiendo sus medidas
        paperectMM = printer.pageRect(QPrinter.Millimeter)
        paperectPixel = printer.pageRect(QPrinter.DevicePixel)
        comp.render(pdfPainter, paperectPixel, paperectMM)                                                      # Con el compositor, renderiza el pintor con las medidas y lo cierra
        pdfPainter.end()
        return True

def printingComposer(ifacex, lyr_lst, templax, width=222, height=185):                                          # <<<Función printingComposer>>>
    map_render = ifacex.mapCanvas().mapSettings()                                                               # Llama al renderizador del canvas
    map_render.setLayers(lyr_lst)                                                                               # Define las capas del renderizador
    dpi = 300 / 25.4                                                                                            # Establece la resolución del renderizador en base a las medidas provistas
    map_render.setOutputSize(QSize(int(width * dpi), int(height * dpi)))
    comp = QgsComposition(map_render)                                                                           # Llama al compositor con el renderizador y establece el estilo de impresión
    comp.setPlotStyle(QgsComposition.Print)
    filex = file(templax, 'rt')                                                                                 # Toma los contenidos de la plantilla y los carga al compositor
    content = filex.read()
    filex.close()
    docx = QDomDocument()
    docx.setContent(content)
    comp.loadFromTemplate(docx)
    return comp                                                                                                 # Retorna el compositor de impresión

def setComposerStdFeats(comp, titlex, lyr_lst, ext, scale_lst):                                                 # <<<Función setComposerStdFeats>>>
    arrowx = comp.getComposerItemById("arrow")                                                                  # Muestra la flecha apuntando al Norte
    arrowx.setPicturePath(":/icons/resources/icons/arrow.png")
    hoy = date.today()                                                                                          # Edita la etiqueta de la fecha con la actual
    datex = comp.getComposerItemById("today_date")
    datex.setText("FECHA: " + str(hoy.day) + "/" + str(hoy.month) + "/" + str(hoy.year))
    titl = comp.getComposerItemById("title")                                                                    # Asigna el título de la plantilla
    titl.setText(titlex)
    scalebar = comp.getComposerItemById("scale_bar")                                                            # Toma la barra de escala y el mapa
    mapx = comp.getComposerItemById("map")
    mapx.setUpdatesEnabled(True)                                                                                # Habilita la actualización del mapa y asigna sus capas
    mapx.setLayerSet(lyr_lst)
    mapx.zoomToExtent(ext)                                                                                      # Ajusta la extensión del mapa en base a la provista y la ajusta con la lista de escalas
    mapx.setNewScale(scale_lst[0])
    scalebar.setNumUnitsPerSegment(scale_lst[0]/20)
    j = 1
    while(j < len(scale_lst) and ((mapx.extent().width() < ext.width()) or
    (mapx.extent().height() < ext.height()))):
        mapx.setNewScale(scale_lst[j])
        scalebar.setNumUnitsPerSegment(scale_lst[j]/20)
        j = j + 1