# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Cencosud
                                 A QGIS plugin
 Procesa listas de clientes aportadas por Cencosud
                              -------------------
        begin                : 2017-04-26
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Ledesma S.A.A.I.
        email                : xxxx@ledesma.com.ar
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Importa las clases de fecha y tiempo
import time
from datetime import date
# Importa las funciones de PyQt y QGIS
from PyQt4 import QtCore
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtXml import *
from qgis.gui import *
from qgis.core import *
from qgis.utils import *
# Inicializa los recursos Qt desde resources_rc.py
import resources_rc
# Importa el codigo del dialogo
from cencosud_dialog import *
# Importa las funciones generales y las de cada funcionalidad
from generalus import *
from sel_clientes import *
# Importa librerias auxiliares
import os.path
import itertools
import sys
import math
import logging
import numbers
from mainwindow import *
sys.path.append('C:\Python27\Lib')
from gettext import *
from functools import partial

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(text, context="Cencosud", disambig=None):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(text, context="Cencosud", disambig=None):
        return QtGui.QApplication.translate(context, text, disambig)

class Cencosud:
    """Implementación del Plugin en QGIS"""
    def __init__(self, iface):
        """Constructor.
        :iface: La instancia de interfaz que provee la posibilidad de manipular la
        aplicación de QGIS durante el tiempo de ejecucion :tipo iface: QgsInterface """
        # Guarda la referencia al interfaz de QGIS
        self.iface = iface
        # Incializa el directorio del plugin
        self.plugin_dir = os.path.dirname(__file__)
        # Incializa locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(self.plugin_dir,'i18n',
            'Cencosud_{}.qm'.format(locale))
        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)
            if qVersion() > '4.3.3': QCoreApplication.installTranslator(self.translator)
        # Ventana y su barra de menú
        self.window = newWindow(u'Cencosud', QIcon(":icons/resources/icons/logo_c.png"))
        # Crea las instancias de los cuadros de diálogo
        self.dlgs = {}
        self.dlgs['selClientes'] = CencosudDlgSelecClientes()
        # Declara la instancia de las acciones
        self.actions = []
        # Declara la instancia logger y el menú desplegable de acciones
        self.logger = None
        self.pop_up_menu = QMenu(self.iface.mainWindow())
        # noinspection PyMethodMayBeStatic
    
    def tr(self, message):
        """Traduce un string a través de Qt translation API. La implementación
        se debe a que no heredamos QObject. Parámetros:
        :message: String a traducir :tipo message: str, QString
        :return: DSevuelve la versión traducida del mensaje :rtype: QString"""
        # no inspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('Cencosud', message)
    
    def addAction(self, icon_path, text, callback, status_tip=None, 
    enabled_flag=True, add_to_pop=True, whats_this=None, parent=None):
        """Define los parámetros de la acción. Parámetros:
        :icon_path: Directorio del ícono de la acción :tipo icon_path: str
        :text: Texto que se muestra en el menú para la acción :tipo text: str
        :callback: Función que se ejecuta luego de la acción :tipo callback: function
        :enabled_flag: Indicador que habilita la acción (por defecto es True).
        :tipo enabled_flag: bool
        :add_to_pop: Un indicador que agrega la acción al menú desplegable (por defecto
        es False). :tipo add_to_pop: bool
        :status_tip: Texto opcional que se muestra cuando el mouse pasa sobre la acción 
        en el menú desplegable (por defecto es None). :tipo status_tip: str
        :parent: Objeto padre de la acción (por defecto es None). :tipo parent: QWidget
        :whats_this: Texto opcional que se muestra en la barra de estado cuado el mouse
        pasa sobre la acción. :tipo status_tip: str
        :return: Devuelve la acción creada que se agrega a self.actions. :rtipo: QAction
        """
        # Define el objeto padre de la acción
        if parent is None: parent=self.iface.mainWindow()
        # Crea la acción a partir de un ícono, texto y función dados
        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        # Habilita la acción
        action.setEnabled(enabled_flag)
        # Define status_tip, whats_this y añade la acción al menú desplegable
        if status_tip is not None: action.setStatusTip(status_tip)
        if whats_this is not None: action.setWhatsThis(whats_this)
        if add_to_pop: self.pop_up_menu.addAction(action)
        return action
    
    def initGui(self):
        """Inicializacion de logging"""
        self.logger = logDate(self.plugin_dir, "Cencosud")
        """Crea las acciones del menú desplegable, con sus funciones e íconos asociados"""
        # Actualizar Capa
        action1 = self.addAction(":/icons/resources/icons/map2.png",
        self.tr(u'Actualizar Capa'), partial(loadTxtFile, self.iface, "Clientes"))
        # Seleccionar Clientes
        action2 = self.addAction(":/icons/resources/icons/map2.png",
        self.tr(u'Seleccionar Clientes'), partial(runDialog, self.dlgs['selClientes']))
        # Mapeo de la capa Locales
        #actiontest = self.addAction(":/icons/resources/icons/map2.png",
        #self.tr(u'Mapeo Locales'), partial(mapeoLocales, self.iface))
        # Ventana con los menues de acciones
        actionw = self.addAction(":/icons/resources/icons/logo_c.png", 
        self.tr(u'Cencosud'), partial(runDialog, self.window, False))
        # Agrega el plugin a la ventana del QGIS
        self.iface.addToolBarIcon(actionw)
        self.iface.addPluginToMenu('&Cencosud', actionw)
        """Menu Clientes de la ventana"""
        clientes = self.window.menuBar().addMenu("Clientes")
        # Acciones de clientes
        clientes.addAction(action1)
        clientes.addAction(action2)
        #clientes.addAction(actiontest)
        """Botones de los cuadros de dialogo"""
        # Seleccionar Clientes
        clientes.aboutToShow.connect(partial(selClientesComboBox, self.iface, self.dlgs))
        self.dlgs['selClientes'].local_nombre.currentIndexChanged['QString'].connect(partial(updateLocalesComboBox,
        self.iface, self.dlgs))
        self.dlgs['selClientes'].selecButton.clicked.connect(partial(selClientesByAttr, self.iface, self.dlgs))
        self.dlgs['selClientes'].exportButton.clicked.connect(partial(exportClientesCheck, self.iface,
        self.dlgs, self.plugin_dir))
        self.dlgs['selClientes'].marmax_check.stateChanged.connect(partial(updateMaxMargin, self.dlgs))
        self.dlgs['selClientes'].marmin_check.stateChanged.connect(partial(updateMinMargin, self.dlgs))
        """Herramientas del mapa"""
        # Herramienta de Vista Mapa Tematico
        #self.maptool1 = connectTool2(self.iface.mapCanvas())
        #self.maptool1.line_complete.connect(self.map_extent2)
        #"""Inicializacion del cuadro de dialogo de mapas tematico"""
        #self.map_theming()
        #self.dlg.frame.setStyleSheet("image: url(:/color_ramps/resources/color_ramps/Blues.svg);")
    
    def unload(self):
        """Elimina el menú y sus acciones de QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(self.tr(u'&Cencosud'), action)
            self.iface.removeToolBarIcon(action)