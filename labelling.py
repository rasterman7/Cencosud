# -*- coding: utf-8 -*-
from PyQt4 import QtCore                                                                                        # Importa las librerias de PyQt y QGIS
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtXml import *
from qgis.core import *
from qgis.gui import *
from qgis.utils import *

def editLyrLabelProp(layerx, field, prop, exp):                                                                 # <<<Función editLyrLabelProp>>>
    palyr = QgsPalLayerSettings()                                                                               # Edita una determinada propiedad de la etiqueta de la capa con una expresión
    palyr.readFromLayer(layerx)
    palyr.enabled = True
    palyr.setDataDefinedProperty(prop, True, True, exp, '')
    palyr.writeToLayer(layerx)

def editLyrLabelSettings1(layerx, field, prop_dic, placex=QgsPalLayerSettings.AroundPoint,                      # <<<Función editLyrLabelSettings1>>>
    quad=QgsPalLayerSettings.QuadrantAbove, shapex=None):
    palyr = QgsPalLayerSettings()                                                                               # Abre una instancia de configuración de etiquetas
    palyr.readFromLayer(layerx)                                                                                 # Toma la capa a etiquetar
    palyr.enabled = True                                                                                        # Habilita las etiquetas
    palyr.drawLabels = True
    palyr.bufferDraw = True                                                                                     # Habilita el buffer (reborde) de la etiqueta
    palyr.fieldName = field                                                                                     # Define el atributo de la etiqueta, su ubicación y cuadrante
    palyr.placement = placex
    palyr.quadOffset = quad
    if shapex is not None:                                                                                      # Si recibe una forma, la habilita, asigna su geometría y su formato (preestablecido)
        palyr.shapeDraw = True
        palyr.shapeType = shapex
        palyr.shapeFillColor = QColor("white")
        palyr.shapeRadii = QPointF(10.0, 10.0)
        palyr.shapeBorderColor = QColor("black")
        palyr.shapeBorderWidth = 0.3
    for k in prop_dic.keys(): palyr.setDataDefinedProperty(k, True, True, prop_dic[k], '')                      # Asigna la expresión asociada a cada característica definida en el diccionario
    palyr.writeToLayer(layerx)                                                                                  # Guarda la configuración de la etiqueta

def enableLyrLayer(layerx, t=False):                                                                            # <<<Función enableLyrLayer>>>
    palyr = QgsPalLayerSettings()                                                                               # Habilita o deshabilita el etiquetado de una capa vectorial dada
    palyr.readFromLayer(layerx)
    palyr.enabled = t
    palyr.writeToLayer(layerx)

def genLabelExp(field=None, value=None, intflag=False):                                                         # <<<Función genLabelExp>>>
    exp = None                                                                                                  # Genera la expresión de la etiqueta para un campo y su valor
    if (value is not None):
        if intflag: exp = u'"{0}" = {1}'.format(field, unicode(value))
        else: exp = u'"{0}" = \'{1}\''.format(field, unicode(value))
    return exp

def genLabelExpForFeat(layerx, feat):                                                                           # <<<Función genLabelExpForFeat>>>
    exp = ''                                                                                                    # Genera una expresión tal que sólo se corresponda con el objeto dado de la capa
    for f in layerx.pendingFields().toList():
        if exp == '':
            if (u'Integer' in f.typeName()) or (u'Real' == f.typeName()):
                exp = genLabelExp(f.name(), feat.attribute(f.name()), True)
            else: exp = genLabelExp(f.name(), feat.attribute(f.name()))
        else:
            if (u'Integer' in f.typeName()) or (u'Real' == f.typeName()):
                exp = exp + ' AND ' + genLabelExp(f.name(), feat.attribute(f.name()), True)
            else: exp = exp + ' AND ' + genLabelExp(f.name(), feat.attribute(f.name()))
    return exp

def isLyrLabelEnabled(layerx):                                                                                  # <<<Función isLyrLabelEnabled>>>
    palyr = QgsPalLayerSettings()                                                                               # Abre la configuración de etiquetas y se fija si está habilitada
    palyr.readFromLayer(layerx)
    return palyr.enabled

def verifyLabelExp(exp=None):                                                                                   # <<<Función verifyLabelExp>>>
    if exp is None: return None                                                                                 # Verifica la validez de la expresión y muestra sus errores, en caso de existir
    verif = QgsExpression(exp)
    if verif.hasParserError(): raise Exception(verif.parserErrorString())
    v = verif.evaluate()
    if verif.hasEvalError(): raise ValueError(verif.evalErrorString())
    return verif