import os
import sys

# Imnporta la funcion gettext
sys.path.append('C:\Python27\Lib')
from gettext import *

# Importa las funciones de PyQt y QGIS
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from qgis.core import *
from qgis.gui import *
from qgis.utils import *
# Inicializa los recursos Qt desde resources_rc.py
import resources_rc

class OurMainWindow(QMainWindow):
    plugin_dir = os.path.dirname(__file__)
    def __init__(self):
        # Incializa la ventana
        QMainWindow.__init__(self)
        # Establece el GUI
        self.setupGUI()
        
    def setupGUI(self):
        # Crea el recuadro y lo establece como objeto principal de la ventana
        frame = QFrame(self)
        self.setCentralWidget(frame)
        # Establece la disposicion como tabla del recuadro
        self.grid_layout = QGridLayout(frame)
        # Define el canvas de la ventana de color blanco
        self.map_canvas = QgsMapCanvas()
        self.map_canvas.setCanvasColor(QColor(255, 255, 255))
        self.grid_layout.addWidget(self.map_canvas)
        # Establece las acciones de acercar, alejar, mover y extension total
        zoomin_act = QAction(QIcon(":/window/resources/window/zoomin.png"), 'Acercar', self)
        zoomout_act = QAction(QIcon(":/window/resources/window/zoomout.png"), 'Alejar', self)
        pan_act = QAction(QIcon(":/window/resources/window/pan.png"), 'Mover', self)
        zoomf_act = QAction(QIcon(":/window/resources/window/zoomextent.png"), 'Extension total', self)
        # Establece las acciones de abrir capa y guarda extension como imagen
        open_act = QAction(QIcon(":/window/resources/window/open.png"), 'Abrir Capa', self)
        save_act = QAction(QIcon(":/window/resources/window/file-save.png"), 'Guardar como imagen', self)
        # Crea la barra de herramientas 'Herramientas' y agrega las acciones anteriores
        self.toolbar = self.addToolBar('Herramientas')
        self.toolbar.addAction(zoomin_act)
        self.toolbar.addAction(zoomout_act)
        self.toolbar.addAction(pan_act)
        self.toolbar.addAction(zoomf_act)
        self.toolbar.addAction(open_act)
        self.toolbar.addAction(save_act)
        # Conecta las acciones con su respectivo metodo
        zoomin_act.triggered.connect(self.zoom_in)
        zoomout_act.triggered.connect(self.zoom_out)
        pan_act.triggered.connect(self.pan)
        zoomf_act.triggered.connect(self.map_canvas.zoomToFullExtent)
        open_act.triggered.connect(self.open_tool)
        save_act.triggered.connect(self.save_image_tool)
        # Crea las herramientas de acercar, alejar y mover
        self.tool_zoomin = QgsMapToolZoom(self.map_canvas, False)
        self.tool_zoomout = QgsMapToolZoom(self.map_canvas, True)
        self.tool_pan = QgsMapToolPan(self.map_canvas)
        
    def add_ogr_layer(self, path):
        # Se define el nombre de la capa
        (name, ext) = os.path.basename(path).split('.')
        v = QgsVectorLayer(path, name, 'ogr')
        # Se verifica la validez de la capa
        if not v.isValid():
            self.error_msg("La carga de la capa vectorial fallo")
            return None
        else:
            # Se agrega la capa al canvas
            QgsMapLayerRegistry.instance().addMapLayer(v)
            self.add_layer(v)
            return v
        
    def add_layer(self, layer, list=None):
        # Define la extension del mapa
        self.map_canvas.setExtent(layer.extent())
        # Toma la lista de capas y agrega la siguiente
        if list is None:
            list = []
            for i in self.map_canvas.layers():
                list.append(QgsMapCanvasLayer(i))
        list.append(QgsMapCanvasLayer(layer))
        # Establece la lista nueva de capas
        self.map_canvas.setLayerSet(list)
        
    def error_msg(self, text):
        # Mensaje de error
        self.iface.messageBar().pushMessage("Error", text, QgsMessageBar.CRITICAL, 3)
        
    def zoom_in(self):
        # Establece la herramienta de acercamiento
        self.map_canvas.setMapTool(self.tool_zoomin)
        
    def zoom_out(self): 
        # Establece la herramineta de alejamiento
        self.map_canvas.setMapTool(self.tool_zoomout)
        
    def pan(self): 
        # Establece la herramineta de paneo
        self.map_canvas.setMapTool(self.tool_pan)
    
    def open_tool(self):
        # Crea el cuadro de dialogo para seleccionar el archivo
        file = QFileDialog(self)
        filterx = NullTranslations().ugettext("Shapefile(*.shp *.SHP);;All Files(*.*)")
        # El usuario selecciona el archivo
        x = file.getOpenFileName(caption = u'Abrir Capa', filter = filterx)
        try:
            # Agrega la capa a la ventana
            self.add_ogr_layer(x)
            # Establece la extension de la ventana a la de la capa
            self.map_canvas.zoomToFullExtent()
        except: pass
    
    def save_image_tool(self):
        # Crea el cuadro de dialogo para seleccionar el archivo
        file = QFileDialog(self)
        # El usuario elige el directorio de la imagen
        x = file.getSaveFileName(filter = NullTranslations().ugettext("Portable Networks" +
        " Graphics(*.png *.PNG);;JPG(*.jpg *.jpeg *.JPG *.JPEG);;All Files(*.*)"),
        caption = u'Guarda extension como imagen')
        try:
            # Toma el directorio y guarda la extension como imagen
            (name, ext) = os.path.basename(x).split('.')
            self.map_canvas.saveAsImage(x, None, ext)
        except: pass
        
class OurMenuBar(QMenuBar):
    def __init__(self):
        # Inicializa la barra de menu
        QMenuBar.__init__(self)
        
class connectTool(QgsMapToolEmitPoint):
    """Herramienta para emitir lineas de puntos"""
    # Se crea la senal para emitir los puntos
    line_complete = pyqtSignal(list)
    # Se crea el resto de las variables
    line = []
    start_point = None
    end_point = None
    rubber_band = None
    calle = None
    
    def __init__(self, canvas):
        # Se inicializa el canvas
        self.canvas = canvas
        # Se crea la variable de autoensamblado
        self.utilings = QgsSnappingUtils()
        QgsMapToolEmitPoint.__init__(self, canvas)
        # Se asigna la senal de desactivado a una funcion
        self.deactivated.connect(self.dect)
        
    def canvasMoveEvent(self, event):
        # Se obtienen los datos del punto de posicion del raton
        point = self.toMapCoordinates(event.pos())
        # Detecta si la linea ya tenia un punto inicial
        if self.start_point:
            # Detecta si se creo la linea
            if self.rubber_band:
                self.rubber_band.reset()
            else:
                # Crea la variable de la linea y le asigna color
                self.rubber_band = QgsRubberBand(self.canvas, False)
                self.rubber_band.setColor(QColor.fromRgb(255, 0, 0, 100))
            # Establece la geometry de la linea
            points = self.line[:] + [point]
            self.rubber_band.setToGeometry(QgsGeometry.fromPolyline(points), None)
    
    def canvasPressEvent(self, event):
        # Se obtienen los datos del punto de posicion del raton
        point2 = self.toMapCoordinates(event.pos())
        # Detecta si se encontro la capa 'Calles'
        if self.calle is not None:
            # Localiza el punto mas cercano en una distancia
            t = self.canvas.snappingUtils().locatorForLayer(self.calle)
            m = t.nearestVertex(point2, 5)
            # Si encuentra el punto, toma sus datos, reemplazando los anteriores
            if m.isValid():
                point2 = m.point()
        # Si no existe el punto inicial, toma el actual
        if self.start_point is None:
            self.start_point = point2
            # Se agregan los datos del punto a la lista de puntos de la linea
            self.line = [self.start_point]
        else:
            # Agrega el punto a la lista de la linea
            self.line.append(point2)
            # Redefine la geometria de la linea con la nueva lista de puntos
            self.rubber_band.reset()
            self.rubber_band.setToGeometry(QgsGeometry.fromPolyline(self.line), None)
            
    def keyPressEvent(self, event):
        # Detecta si la tecla ingresada fue 'Enter'
        if event.key() == 0x01000004:
            # Emite la senal con la lista de puntos
            self.line_complete.emit(self.line)
            # Llama a la funcion de reinicio
            self.dect()
        # Detecta si la tecla ingresada fue 'Escape'
        if event.key() == 0x01000000:
            # Llama a la funcion de reinicio
            self.dect()
            
    def dect(self):
        # Se reinicia la geometria de la linea
        if self.rubber_band is not None:
            self.rubber_band.reset()
        # Se vacian las variables
        self.start_point = None
        self.end_point = None
        self.line[:] = []
        
class connectTool2(connectTool):
    """Herramienta para emitir poligonos"""
    def __init__(self, canvas):
        # Se inicializa el canvas
        self.canvas = canvas
        # Se crea la variable de autoensamblado
        self.utilings = QgsSnappingUtils()
        QgsMapToolEmitPoint.__init__(self, canvas)
        # Se asigna la senal de desactivado a una funcion
        self.deactivated.connect(self.dect)
    
    def canvasMoveEvent(self, event):
        # Se recorre el listado de capas en busca de la capa de 'Calles'
        for i in iface.mapCanvas().layers():
            if i.name() == u'Calles':
                self.calle = i
        # Se obtienen los datos del punto de posicion del raton
        point = self.toMapCoordinates(event.pos())
        # Detecta si la linea ya tenia un punto inicial
        if self.start_point:
            # Detecta si se creo la linea o el poligono
            if self.rubber_band:
                self.rubber_band.reset()
            else:
                # Crea la variable de la linea y le asigna color
                self.rubber_band = QgsRubberBand(self.canvas, False)
                self.rubber_band.setColor(QColor.fromRgb(255, 0, 0, 100))
            # Establece la geometria de la linea
            points = self.line[:] + [point]
            # Transforma la linea en poligono si se tiene mas de dos puntos
            if len(self.line) < 2:
                self.rubber_band.setToGeometry(QgsGeometry.fromPolyline(points), None)
            else: self.rubber_band.setToGeometry(QgsGeometry.fromPolygon([points]), None)
            
            
    def canvasPressEvent(self, event):
        # Se obtienen los datos del punto de posicion del raton
        point2 = self.toMapCoordinates(event.pos())
        # Detecta si se encontro la capa 'Calles'
        if self.calle is not None:
            # Localiza el punto mas cercano en una distancia
            t = self.canvas.snappingUtils().locatorForLayer(self.calle)
            m = t.nearestVertex(point2, 5)
            # Si encuentra el punto, toma sus datos, reemplazando los anteriores
            if m.isValid():
                point2 = m.point()
        # Si no existe el punto inicial, toma el actual
        if self.start_point is None:
            self.start_point = point2
            # Se agregan los datos del punto a la lista de puntos de la linea
            self.line = [self.start_point]
        else:
            # Agrega el punto a la lista de la linea
            self.line.append(point2)
            # Redefine la geometria de la linea con la nueva lista de puntos
            self.rubber_band.reset()
            # Transforma la linea en poligono si se tiene mas de dos puntos
            if len(self.line) < 2:
                self.rubber_band.setToGeometry(QgsGeometry.fromPolyline(self.line), None)
            else: self.rubber_band.setToGeometry(QgsGeometry.fromPolygon([self.line]), None)