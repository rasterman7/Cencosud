# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Cencosud
                                 A QGIS plugin
 Procesa listas de clientes aportadas por Cencosud
                             -------------------
        begin                : 2017-04-26
        copyright            : (C) 2017 by Ledesma S.A.A.I.
        email                : xxxx@ledesma.com.ar
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load Cencosud class from file Cencosud.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .cencosud import Cencosud
    return Cencosud(iface)
