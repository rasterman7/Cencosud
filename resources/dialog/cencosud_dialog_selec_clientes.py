# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'cencosud_dialog_selec_clientes.ui'
#
# Created: Sat Aug 19 10:21:59 2017
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_CencosudDialogBase(object):
    def setupUi(self, CencosudDialogBase):
        CencosudDialogBase.setObjectName(_fromUtf8("CencosudDialogBase"))
        CencosudDialogBase.resize(260, 410)
        CencosudDialogBase.setMinimumSize(QtCore.QSize(260, 400))
        CencosudDialogBase.setMaximumSize(QtCore.QSize(16777215, 410))
        self.gridLayout = QtGui.QGridLayout(CencosudDialogBase)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.clientes_7 = QtGui.QLabel(CencosudDialogBase)
        self.clientes_7.setObjectName(_fromUtf8("clientes_7"))
        self.gridLayout.addWidget(self.clientes_7, 12, 0, 1, 2)
        self.clientes_5 = QtGui.QLabel(CencosudDialogBase)
        self.clientes_5.setObjectName(_fromUtf8("clientes_5"))
        self.gridLayout.addWidget(self.clientes_5, 10, 0, 1, 2)
        self.clientes_estado = QtGui.QComboBox(CencosudDialogBase)
        self.clientes_estado.setMinimumSize(QtCore.QSize(0, 20))
        self.clientes_estado.setObjectName(_fromUtf8("clientes_estado"))
        self.gridLayout.addWidget(self.clientes_estado, 10, 2, 1, 4)
        self.local_1 = QtGui.QLabel(CencosudDialogBase)
        self.local_1.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.local_1.setObjectName(_fromUtf8("local_1"))
        self.gridLayout.addWidget(self.local_1, 3, 0, 1, 6)
        self.clientes_6 = QtGui.QLabel(CencosudDialogBase)
        self.clientes_6.setObjectName(_fromUtf8("clientes_6"))
        self.gridLayout.addWidget(self.clientes_6, 11, 0, 1, 2)
        self.local_nombre = QtGui.QComboBox(CencosudDialogBase)
        self.local_nombre.setMinimumSize(QtCore.QSize(0, 20))
        self.local_nombre.setObjectName(_fromUtf8("local_nombre"))
        self.gridLayout.addWidget(self.local_nombre, 4, 2, 1, 4)
        self.local_2 = QtGui.QLabel(CencosudDialogBase)
        self.local_2.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.local_2.setObjectName(_fromUtf8("local_2"))
        self.gridLayout.addWidget(self.local_2, 4, 0, 1, 2)
        self.title = QtGui.QLabel(CencosudDialogBase)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.title.setFont(font)
        self.title.setAlignment(QtCore.Qt.AlignCenter)
        self.title.setObjectName(_fromUtf8("title"))
        self.gridLayout.addWidget(self.title, 0, 0, 3, 6)
        self.clientes_8 = QtGui.QLabel(CencosudDialogBase)
        self.clientes_8.setObjectName(_fromUtf8("clientes_8"))
        self.gridLayout.addWidget(self.clientes_8, 13, 0, 1, 2)
        self.selecButton = QtGui.QPushButton(CencosudDialogBase)
        self.selecButton.setMinimumSize(QtCore.QSize(0, 20))
        self.selecButton.setObjectName(_fromUtf8("selecButton"))
        self.gridLayout.addWidget(self.selecButton, 14, 4, 1, 2)
        self.clientes_1 = QtGui.QLabel(CencosudDialogBase)
        self.clientes_1.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.clientes_1.setObjectName(_fromUtf8("clientes_1"))
        self.gridLayout.addWidget(self.clientes_1, 6, 0, 1, 6)
        self.clientes_2 = QtGui.QLabel(CencosudDialogBase)
        self.clientes_2.setObjectName(_fromUtf8("clientes_2"))
        self.gridLayout.addWidget(self.clientes_2, 7, 0, 1, 2)
        self.local_3 = QtGui.QLabel(CencosudDialogBase)
        self.local_3.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.local_3.setObjectName(_fromUtf8("local_3"))
        self.gridLayout.addWidget(self.local_3, 5, 0, 1, 2)
        self.clientes_3 = QtGui.QLabel(CencosudDialogBase)
        self.clientes_3.setObjectName(_fromUtf8("clientes_3"))
        self.gridLayout.addWidget(self.clientes_3, 8, 0, 1, 2)
        self.clientes_cadena = QtGui.QComboBox(CencosudDialogBase)
        self.clientes_cadena.setMinimumSize(QtCore.QSize(0, 20))
        self.clientes_cadena.setObjectName(_fromUtf8("clientes_cadena"))
        self.gridLayout.addWidget(self.clientes_cadena, 8, 2, 1, 4)
        self.exportacion = QtGui.QLabel(CencosudDialogBase)
        self.exportacion.setObjectName(_fromUtf8("exportacion"))
        self.gridLayout.addWidget(self.exportacion, 15, 0, 1, 6)
        self.local_cadena = QtGui.QLineEdit(CencosudDialogBase)
        self.local_cadena.setMinimumSize(QtCore.QSize(0, 20))
        self.local_cadena.setObjectName(_fromUtf8("local_cadena"))
        self.gridLayout.addWidget(self.local_cadena, 5, 2, 1, 4)
        self.clientes_4 = QtGui.QLabel(CencosudDialogBase)
        self.clientes_4.setObjectName(_fromUtf8("clientes_4"))
        self.gridLayout.addWidget(self.clientes_4, 9, 0, 1, 2)
        self.clientes_idrango = QtGui.QComboBox(CencosudDialogBase)
        self.clientes_idrango.setMinimumSize(QtCore.QSize(0, 20))
        self.clientes_idrango.setObjectName(_fromUtf8("clientes_idrango"))
        self.gridLayout.addWidget(self.clientes_idrango, 9, 2, 1, 4)
        self.clientes_local = QtGui.QComboBox(CencosudDialogBase)
        self.clientes_local.setMinimumSize(QtCore.QSize(0, 20))
        self.clientes_local.setObjectName(_fromUtf8("clientes_local"))
        self.gridLayout.addWidget(self.clientes_local, 7, 2, 1, 4)
        self.clientes_radio = QtGui.QComboBox(CencosudDialogBase)
        self.clientes_radio.setMinimumSize(QtCore.QSize(0, 20))
        self.clientes_radio.setObjectName(_fromUtf8("clientes_radio"))
        self.clientes_radio.addItem(_fromUtf8(""))
        self.clientes_radio.addItem(_fromUtf8(""))
        self.gridLayout.addWidget(self.clientes_radio, 13, 2, 1, 4)
        self.exportRadio1 = QtGui.QRadioButton(CencosudDialogBase)
        self.exportRadio1.setObjectName(_fromUtf8("exportRadio1"))
        self.gridLayout.addWidget(self.exportRadio1, 16, 0, 1, 4)
        self.exportRadio2 = QtGui.QRadioButton(CencosudDialogBase)
        self.exportRadio2.setObjectName(_fromUtf8("exportRadio2"))
        self.gridLayout.addWidget(self.exportRadio2, 17, 0, 1, 4)
        self.exportButton = QtGui.QPushButton(CencosudDialogBase)
        self.exportButton.setMinimumSize(QtCore.QSize(0, 20))
        self.exportButton.setObjectName(_fromUtf8("exportButton"))
        self.gridLayout.addWidget(self.exportButton, 17, 4, 1, 2)
        self.marmax_check = QtGui.QCheckBox(CencosudDialogBase)
        self.marmax_check.setObjectName(_fromUtf8("marmax_check"))
        self.gridLayout.addWidget(self.marmax_check, 12, 5, 1, 1)
        self.marmax_line = QtGui.QLineEdit(CencosudDialogBase)
        self.marmax_line.setObjectName(_fromUtf8("marmax_line"))
        self.gridLayout.addWidget(self.marmax_line, 12, 2, 1, 3)
        self.marmin_line = QtGui.QLineEdit(CencosudDialogBase)
        self.marmin_line.setObjectName(_fromUtf8("marmin_line"))
        self.gridLayout.addWidget(self.marmin_line, 11, 2, 1, 3)
        self.marmin_check = QtGui.QCheckBox(CencosudDialogBase)
        self.marmin_check.setChecked(False)
        self.marmin_check.setObjectName(_fromUtf8("marmin_check"))
        self.gridLayout.addWidget(self.marmin_check, 11, 5, 1, 1)

        self.retranslateUi(CencosudDialogBase)
        QtCore.QMetaObject.connectSlotsByName(CencosudDialogBase)

    def retranslateUi(self, CencosudDialogBase):
        CencosudDialogBase.setWindowTitle(_translate("CencosudDialogBase", "C - Seleccionar Clientes", None))
        self.clientes_7.setText(_translate("CencosudDialogBase", "Margen máx:", None))
        self.clientes_5.setText(_translate("CencosudDialogBase", "Estado:", None))
        self.local_1.setText(_translate("CencosudDialogBase", "LOCAL", None))
        self.clientes_6.setText(_translate("CencosudDialogBase", "Margen mín:", None))
        self.local_2.setText(_translate("CencosudDialogBase", "Nombre:", None))
        self.title.setText(_translate("CencosudDialogBase", "Seleccionar Clientes", None))
        self.clientes_8.setText(_translate("CencosudDialogBase", "Radio:", None))
        self.selecButton.setText(_translate("CencosudDialogBase", "Seleccionar", None))
        self.clientes_1.setText(_translate("CencosudDialogBase", "CLIENTES", None))
        self.clientes_2.setText(_translate("CencosudDialogBase", "Local:", None))
        self.local_3.setText(_translate("CencosudDialogBase", "Cadena:", None))
        self.clientes_3.setText(_translate("CencosudDialogBase", "Cadena:", None))
        self.exportacion.setText(_translate("CencosudDialogBase", "EXPORTACIÓN", None))
        self.clientes_4.setText(_translate("CencosudDialogBase", "idrango:", None))
        self.clientes_radio.setItemText(0, _translate("CencosudDialogBase", "1 km", None))
        self.clientes_radio.setItemText(1, _translate("CencosudDialogBase", "3 km", None))
        self.exportRadio1.setText(_translate("CencosudDialogBase", "Exportar a CSV", None))
        self.exportRadio2.setText(_translate("CencosudDialogBase", "Exportar a Mapa temático", None))
        self.exportButton.setText(_translate("CencosudDialogBase", "Exportar", None))
        self.marmax_check.setText(_translate("CencosudDialogBase", "Indistinto", None))
        self.marmin_check.setText(_translate("CencosudDialogBase", "Indistinto", None))

