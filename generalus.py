# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui                                                                                 # Importa las librerias de PyQt y QGIS
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtXml import *
from qgis.gui import *
from qgis.core import *
from qgis.utils import *
import time                                                                                                     # Importa las librerias de fecha y tiempo
from datetime import date
import logging                                                                                                  # Importa las librerias de registro y direccionamiento
import os.path
from labelling import *                                                                                         # Importa las librerias de etiquetado y localización
from gettext import *

logger = None                                                                                                   # Variables globales de logging
flagger = True

try:                                                                                                            # <<<Función _translate>>> definida para la traducción de texto en codificación UTF-8
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(text, context="Cencosud", disambig=None):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(text, context="Cencosud", disambig=None):
        return QtGui.QApplication.translate(context, text, disambig)

def attrToComboBox(combox, lyrx, attx):                                                                         # <<<Función attrToComboBox>>>
    lstx = listDistinct(lyrx, attx)                                                                             # Genera una lista con todas las variantes de un atributo dado en la capa
    while combox.count() != 0: combox.removeItem(0)                                                             # Vacía la lista desplegable y la vuelve a llenar con la generada, evitando los valores nulos
    for l in lstx:
        if not ("NULL" in unicode(l)): combox.addItem(l)

def attrToComboBoxCheck(ifacex, namex, combox, attx):
    lyr = obtainLyr(ifacex, namex)                                                                              # Verifica la presencia de la capa dada
    if lyr is not None: attrToComboBox(combox, lyr, attx)                                                       # Si está, actualiza sus valores en la lista desplegable

def changeLyrColor(ifacex, namex, colorx=None, alpha=None, fillcol=None):                                       # <<<Función changeLyrColor>>>
    lyr = obtainLyr(ifacex, namex)                                                                              # Verifica la presencia de la capa a modificar
    if lyr is not None:
        rend = lyr.rendererV2()                                                                                 # Si está, llama a su renderizador y a una instancia de contexto (agregado en la nueva versión)
        ctx = QgsRenderContext()
        l = rend.symbols2(ctx)                                                                                  # Toma una lista con los símbolos del renderizador y los recorre
        for s in l:
            lst = s.symbolLayers()                                                                              # Toma una lista con los símbolos de capa de cada símbolo y los recorre
            for i in range(len(lst)):
                j = lst[i].clone()                                                                              # Clona el símbolo de capa iterado
                color = j.color()                                                                               # Toma el color inicial del clon
                if colorx is not None:                                                                          # Si el color fue definido válidamente como RGB o texto, se lo asigna al símbolo clon
                    if ',' in colorx:
                        (red, green, blue, alpha) = colorx.split(',')
                        color = QColor.fromRgb(int(red), int(green), int(blue), int(alpha))
                    else: color = QColor(colorx)
                    if color.isValid(): j.setColor(color)
                if alpha is not None:                                                                           # Si la transparencia fue definida, se asigna al clon
                    color.setAlpha(alpha)
                    j.setColor(color)
                if fillcol is not None: j.setFillColor(QColor(fillcol))                                         # Si el color de relleno fue definido, se lo asigna al clon
                s.changeSymbolLayer(i, j)                                                                       # Cambia el valor del símbolo de la capa iterado en base a su clon
    refreshAll(ifacex)                                                                                          # Actualiza el mapa

def copyMemLyrSelected(lyr, namex=None, crsid=4130, crstype="espg"):                                            # <<<Función copyMemLyrSelected>>>
    if (lyr.geometryType() == QGis.Point): geo = "Point"                                                        # Toma el tipo geométrico de la capa
    elif (lyr.geometryType() == QGis.Line): geo = "LineString"
    else: geo = "Polygon"
    if crstype == "espg":                                                                                       # Genera la identificación de un sistema de coordenadas acorde a su tipo y número
        crs = QgsCoordinateReferenceSystem(crsid, QgsCoordinateReferenceSystem.EpsgCrsId).toWkt()
    elif crstype == "internal":
        crs = QgsCoordinateReferenceSystem(crsid, QgsCoordinateReferenceSystem.InternalCrsId).toWkt()
    else: crs = QgsCoordinateReferenceSystem(crsid).toWkt()
    if namex is None: namex = lyr.name() + "_copy"                                                              # Asigna el nombre de la capa de memoria en caso de no ser ingresado
    v = QgsVectorLayer(geo + "?crs=" + crs, namex, "memory")                                                    # Crea una nueva capa vectorial de memoria con los datos anteriores
    v.startEditing()                                                                                            # Edita la capa, asignándole sus atributos y los objetos seleccionados de la capa de referencia
    pr = v.dataProvider()
    pr.addAttributes(lyr.pendingFields().toList())
    lst = lyr.selectedFeatures()
    pr.addFeatures(lst)
    v.commitChanges()
    return v                                                                                                    # Devuelve la capa de memoria creada

def featListToId(lst):                                                                                          # <<<Función featListToId>>>
    lstx = []                                                                                                   # Convierte una lista de objetos en una lista de sus identificadores y la devuelve
    for f in lst: lstx.append(f.id())
    return lstx

def fileDlg(ifacex, name="", filter="Shapefile(*.shp *.SHP);;", save=False):                                    # <<<Función fileDlg>>>
    file = QFileDialog(ifacex.mainWindow())                                                                     # Crea el cuadro de dialogo para seleccionar la ruta de un archivo
    if save is True: captionx = "Guardar " + name                                                               # Establece su título según la operación indicada
    else: captionx = "Abrir " + name
    filterx = NullTranslations().ugettext(filter + "All Files(*.*)")                                            # Establece su filtro según el tipo de archivo indicado
    if save is True: x = file.getSaveFileName(caption = captionx, filter = filterx)                             # Abre el cuadro y obtiene la ruta del archivo (para su lectura o escritura), la cual devuelve posteriormente
    else: x = file.getOpenFileName(caption = captionx, filter = filterx)
    return x

def listDistinct(layerx, attributx):                                                                            # <<<Función listDistinct>>>
    lst = []                                                                                                    # Devuelve una lista sin repeticiones de todos los valores de un atributo en una capa
    for i in layerx.getFeatures():
        name = i.attribute(attributx)
        if not (name in lst): lst.append(name)
    return lst

def loadTxtFile(ifacex, namex):                                                                                 # <<<Función loadTxtFile>>>
    pathx = fileDlg(ifacex, namex, "Archivo de texto (*.txt *.TXT);;")                                          # Toma la ruta del archivo de texto a cargar como capa
    if pathx == '': return                                                                                      # Si el cuadro de diálogo fue cerrado, termina la función
    (filename, ext) = os.path.basename(pathx).split('.')                                                        # Sino, genera una capa vectorial con la ruta, un sistema de referencia de coordenadas y un delimitador 
    lyr = QgsVectorLayer("file:///" + pathx + ("?crs={0}&delimiter={1}&xField={2}" +
    "&yField={3}").format("epsg:4130","%5Ct;","x", "y"), namex, "delimitedtext")
    if lyr.isValid():                                                                                           # Verifica la validez de la capa vectorial generada
        lyr2 = obtainLyr(ifacex, namex)                                                                         # De ser válida elimina alguna capa homónima presente en el proyecto
        if lyr2 is not None: QgsMapLayerRegistry.instance().removeMapLayer(lyr2.id())
        QgsMapLayerRegistry.instance().addMapLayer(lyr)                                                         # Agrega la capa generada al proyecto y actualiza el mapa
        refreshAll(ifacex)

def logDate(plugin_dir, pluginame):                                                                             # <<<Función logDate>>>
    global flagger, logger
    if flagger:                                                                                                 # Verifica si la variable logger no fue inicializada
        hoy = str(date.today().day) + '_' + str(date.today().month) + '_' + str(date.today().year)              # De no ser así, la inicializa en base con la fecha actual del sistema y el nombre del plugin
        logger = logging.getLogger(hoy + pluginame)
        logger.setLevel(logging.DEBUG)                                                                          # Asigna el nivel de mensajes a DEBUG
        try:                                                                                                    # Referencia o genera el archivo .log dentro del directorio /log (lo genera en caso de no existir)
            if os.path.isfile(os.path.join(plugin_dir, 'log', hoy + '.log')): mode = 'a'
            else: mode = 'w'
            filt = logging.FileHandler(os.path.join(plugin_dir, 'log', hoy + '.log'), mode)
        except:
            os.makedirs(os.path.join(plugin_dir, 'log'))
            if os.path.isfile(os.path.join(plugin_dir, 'log', hoy + '.log')): mode = 'a'
            else: mode = 'w'
            filt = logging.FileHandler(os.path.join(plugin_dir, 'log', hoy + '.log'), mode)
        filt.setLevel(logging.DEBUG)                                                                            # Asigna el nivel del filtro a DEBUG
        formt = logging.Formatter('%(asctime)s - %(message)s', '%d/%m/%Y %H:%M:%S')                             # Establece el formato de la fecha
        filt.setFormatter(formt)
        logger.addHandler(filt)
        logger.info('Inicialización\n')                                                                         # Envia el mensaje de inicialización y deshabilita futuras inicializaciones
        flagger = False
    return logger                                                                                               # Devuelve una instancia de la variable logger

def msgError(ifacex, text):                                                                                     # <<<Función msgError>>>
    ifacex.messageBar().pushMessage("Error", _translate(text), QgsMessageBar.CRITICAL, 3)                       # Muestra un mensaje de error en pantalla

def msgInfo(ifacex, theme, text, timer=2):                                                                      # <<<Función msgInfo>>>
    ifacex.messageBar().pushMessage(_translate(theme), _translate(text), QgsMessageBar.INFO, timer)             # Muestra un mensaje de información en pantalla durante un tiempo determinado

def msgWarning(ifacex, theme, text, timer=3):                                                                   # <<<Función msgWarning>>>
    ifacex.messageBar().pushMessage(_translate(theme), _translate(text), QgsMessageBar.WARNING, timer)          # Muestra un mensaje de advertencia en pantalla durante un tiempo determinado

def newMemLyr(geo, namex, fields, crsid=4130, crstype="epsg"):                                                  # <<<Función newMemLyr>>>
    if crstype == "espg":                                                                                       # Genera el identificador de un sistema de coordenadas según su tipo y número
        crs = QgsCoordinateReferenceSystem(crsid, QgsCoordinateReferenceSystem.EpsgCrsId).toWkt()
    elif crstype == "internal":
        crs = QgsCoordinateReferenceSystem(crsid, QgsCoordinateReferenceSystem.InternalCrsId).toWkt()
    else: crs = QgsCoordinateReferenceSystem(crsid).toWkt()
    v = QgsVectorLayer(geo + "?crs=" + crs, namex, "memory")                                                    # Crea una capa vectorial de memoria con los datos de entrada y el generado
    v.startEditing()                                                                                            # Edita la capa, asignándole sus atributos y la devuelve
    pr = v.dataProvider()
    pr.addAttributes(fields)
    v.commitChanges()
    return v

def newWindow(title, icon):                                                                                     # <<<Función newWindow>>>
    window = QMainWindow()                                                                                      # Genera una ventana principal y un cuadro de texto en la misma
    txt = QTextEdit()
    window.setCentralWidget(txt)
    window.setWindowTitle(title)                                                                                # Establece el título, ícono y tamaño de la ventana
    window.setWindowIcon(icon)
    window.setMinimumSize(QSize(400, 100))
    window.setMaximumSize(QSize(600, 200))
    window.menuBar()                                                                                            # Crea las barras de menú y de status para la ventana y la devuelve
    window.statusBar()
    return window

def obtainFeatByAttr(lyr, attr, valuex):                                                                        # <<<Función obtainFeatByAttr>>>
    lst = []                                                                                                    # Filtra en una lista los objetos de la capa por un determinado valor en un atributo dado, y la devuelve
    for f in lyr.getFeatures():
        if f.attribute(attr) == valuex: lst.append(f)
    return lst

def obtainFeatByAttrDic(lyr, dic):                                                                              # <<<Función obtainFeatByAttrDic>>>
    lst = []                                                                                                    # Filtra en una lista los objetos de la capa en base a una serie de valores asociados a atributos, y la devuelve
    for f in lyr.getFeatures():
        flagx = True
        for i in dic.keys():
            if not (f.attribute(i) == dic[i]): flagx = False
        if flagx: lst.append(f)
    return lst

def filterFeatByAttrRange(lst, attr, minvalue=None, maxvalue=None):
    if (minvalue is None) and (maxvalue is None):
        return lst
    else:
        lst2 = []
        for f in lst:
            if (minvalue is None) or (minvalue <= f.attribute(attr)):
                if (maxvalue is None) or (maxvalue >= f.attribute(attr)): lst2.append(f)
        return lst2

def obtainLyr(ifacex, namex):                                                                                   # <<<Función obtainLyr>>>
    lyr = None                                                                                                  # Busca una capa en base a su nombre y la devuelve. Si no existe devuelve una instancia nula
    for l in ifacex.legendInterface().layers():
        if l.name() == namex: lyr = l
    return lyr

def refreshAll(ifacex):                                                                                         # <<<Función refreshAll>>>
    for i in ifacex.legendInterface().layers():                                                                 # Actualiza todas las capas de la intefaz, y el canvas
        ifacex.legendInterface().refreshLayerSymbology(i)
    ifacex.mapCanvas().refresh()

def runDialog(dialog, e_flag=True):                                                                             # <<<Función runDialog>>>
    dialog.hide()                                                                                               # Esconde y muestra el cuadro de diálogo
    dialog.show()
    if e_flag:                                                                                                  # En caso de ser indicado, ejecuta el cuadro de diálogo y lo verifica
        result = dialog._exec()
        if result: pass

def setCRS(ifacex, crsid=4130, crstype="epsg"):                                                                 # <<<Función setCRS>>>
    if crstype == "espg":                                                                                       # Genera una instancia de sistema de coordenadas según su tipo y número
        newCrs = QgsCoordinateReferenceSystem(crsid, QgsCoordinateReferenceSystem.EpsgCrsId)
    elif crstype == "internal":
        newCrs = QgsCoordinateReferenceSystem(crsid, QgsCoordinateReferenceSystem.InternalCrsId)
    else: newCrs = QgsCoordinateReferenceSystem(crsid)
    ifacex.mapCanvas().setCrsTransformEnabled(True)                                                             # Habilita la transformación del sistema de coordenadas del canvas
    canvasCrs = ifacex.mapCanvas().mapSettings().destinationCrs()                                               # Toma el sistema de coordenadas actual y verifica si difiere del actual
    if canvasCrs != newCrs:
        c_trans = QgsCoordinateTransform(canvasCrs, newCrs)                                                     # De ser así, toma una instancia de transformación de coordenadas entre ambos sistemas
        ext = ifacex.mapCanvas().extent()                                                                       # Toma la extensión del mapa y la transforma según el nuevo sistema
        ext = c_trans.transform(ext, QgsCoordinateTransform.ForwardTransform)
        ifacex.mapCanvas().setDestinationCrs(newCrs)                                                            # Establece los nuevos sistema de coordenadas del proyecto, unidades del mapa y extensión del mismo
        ifacex.mapCanvas().setMapUnits(newCrs.mapUnits())
        ifacex.mapCanvas().setExtent(ext)

def setLyrLabelModel(ifacex, layerx, field, intflag, value=None, setexp=None, shape=None,                       # <<<Función setLyrLabelModel>>>
    place=QgsPalLayerSettings.OverPoint, size='8', bu_size='1', bu_color='white'):
    if setexp is not None: exp = setexp                                                                         # Establece la expresión de la etiqueta en caso de recibirla
    else: exp = genLabelExp(field, value)                                                                       # En caso contrario, la genera con el campo y el valor recibidos
    v = verifyLabelExp(exp)                                                                                     # Verifica la validez de la expresión
    if v is None:
        msgError(ifacex, "No se pudo procesar la etiqueta de " + str(layerx.name()))                            # Si es inválida, muestra un mensaje de error y termina la función
        return None
    prop_dic = {QgsPalLayerSettings.Show: exp, QgsPalLayerSettings.Size: size,                                  # Sino genera un diccionario con la expresión que asocie las propiedades con su valor (o expresión)
    QgsPalLayerSettings.BufferSize: bu_size, QgsPalLayerSettings.BufferColor: bu_color}
    editLyrLabelSettings1(layerx, field, prop_dic, placex=place, shapex=shape)                                  # Edita la etiqueta de la capa con el diccionario generado, su campo y el resto de los atributos ingresados
    refreshAll(ifacex)                                                                                          # Actualiza el mapa